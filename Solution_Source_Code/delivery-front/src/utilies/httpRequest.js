import store from "@/store";
import axios from "axios";

const HttpReq = {

    /**
     * GET
     * @param data 
     * @param dir 
     * @returns 
     */
     getData: async function(dir, uri = null) {

        let token = store.getters.getToken
        //variable para obtener la respuesta del servidor
        const resp = {
            data: undefined,
            status: undefined,
            headers: undefined,
            error: undefined
        }

        try {
            let url = process.env.VUE_APP_SERVICE_URL + '/' + dir
            if(uri)
                url =  url + '?'+uri
                
            let response = null
            if(token)
                response = await axios.get(url, { headers: { Authorization: `API ${token}` } })
            else
                response = await axios.get(url)

            resp.data = response.data;
            resp.status = response.status;
            resp.headers = response.headers;
            resp.error = false;
            return resp;
        } catch (error) {
            resp.error = true;
            if (error.response) {
                resp.data = error.response.data;
                resp.status = error.response.status;
                resp.headers = error.response.headers;
            } else if (error.request) {
                resp.status = -1; //Respuesta personalizada, cuando la peticion es hecha pero no se envia respuesta.
                resp.data = "The request was made but no response was received";
            } else {
                resp.status = -2; //Respuesta personalizada
                resp.data = "Something happened in setting up the request that triggered an Error";
            }
            return resp;
        }
    },

    /**
     *  POST
     *  @params data
     *  @params dir
     */
    postData: async function(data, dir) {

        let token = store.getters.getToken

        //variable para obtener la respuesta del servidor
        const resp = {
            data: undefined,
            status: undefined,
            headers: undefined,
            error: undefined
        }

        try {
            const response = await axios.post(process.env.VUE_APP_SERVICE_URL + '/' + dir, data, { headers: { Authorization: `API ${token}` } });
            resp.data = response.data;
            resp.status = response.status;
            resp.headers = response.headers;
            resp.error = false;
            return resp;
        } catch (error) {
            resp.error = true;
            if (error.response) {
                resp.data = error.response.data;
                resp.status = error.response.status;
                resp.headers = error.response.headers;
            } else if (error.request) {
                resp.status = -1; //Respuesta personalizada, cuando la peticion es hecha pero no se envia respuesta.
                resp.data = "The request was made but no response was received";
            } else {
                resp.status = -2; //Respuesta personalizada
                resp.data = "Something happened in setting up the request that triggered an Error";
            }
            return resp;
        }

    },

    /**
     * PUT
     * @param {*} data 
     * @param {*} dir 
     * @param {*} id 
     * @returns 
     */
    putData: async function(data, dir, id) {
        let token = store.getters.getToken
        //variable para obtener la respuesta del servidor
        const resp = {
            data: undefined,
            status: undefined,
            headers: undefined,
            error: undefined
        }

        try {
            const response = await axios.put(process.env.VUE_APP_SERVICE_URL + '/' + dir + '/' + id, data, { headers: { Authorization: `API ${token}` } })
            resp.data = response.data;
            resp.status = response.status;
            resp.headers = response.headers;
            resp.error = false;
            return resp;
        } catch (error) {
            resp.error = true;
            if (error.response) {
                resp.data = error.response.data;
                resp.status = error.response.status;
                resp.headers = error.response.headers;
            } else if (error.request) {
                resp.status = -1; //Respuesta personalizada, cuando la peticion es hecha pero no se envia respuesta.
                resp.data = "The request was made but no response was received";
            } else {
                resp.status = -2; //Respuesta personalizada
                resp.data = "Something happened in setting up the request that triggered an Error";
            }
            return resp;
        }
    },

    /**
     * DELETE
     * @param {*} id 
     * @param {*} dir 
     * @returns 
     */
    deleteData: async function(id, dir) {
        let token = store.getters.getToken
        //Variable para obtener la respuesta del servidor
        const resp = {
            data: undefined,
            status: undefined,
            headers: undefined,
            error: undefined
        }

        try {
            const response = await axios.delete(process.env.VUE_APP_SERVICE_URL + '/' + dir + '/' + id, { headers: { Authorization: `API ${token}` } });
            resp.data = response.data;
            resp.status = response.status;
            resp.headers = response.headers;
            resp.error = false;
            return resp;
        } catch (error) {
            resp.error = true;
            if (error.response) {
                resp.data = error.response.data;
                resp.status = error.response.status;
                resp.headers = error.response.headers;
            } else if (error.request) {
                resp.status = -1; //Respuesta personalizada, cuando la peticion es hecha pero no se envia respuesta.
                resp.data = "The request was made but no response was received";
            } else {
                resp.status = -2; //Respuesta personalizada
                resp.data = "Something happened in setting up the request that triggered an Error";
            }
            return resp;
        }
    }

}

export {HttpReq}