import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store';
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'dashboard',
        component: () => import(/* webpackChunkName: "about" */ '../views/DashboardView.vue'),
        beforeEnter: async (to, from, next) => {
            if (store.getters.userAutenticate) {
                return next()
            }
            if(from.name != 'login')
                return next({ name: 'login'})
            return false

        },
    },
    {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "about" */ '../views/LoginView.vue'),
        beforeEnter: (to, from, next) => {
            if(!store.getters.userAutenticate)
                store.dispatch('loadStorage')
            if (store.getters.userAutenticate) {
                return next({ name: 'dashboard'})
            }
            next()
        },
    },
    {
        path: '/admin',
        component: () => import('../views/AdminView.vue'),
        children: [
            {
                path: '',
                name: 'admin',
                component: () => import('../views/AdminViews/AdminIndex.vue'),
                meta: {guardsRoute: true}
            },
            {
                path: 'users',
                name: 'users',
                component: () => import('@/views/AdminViews/UserAdmin.vue'),
                meta: {guardsRouteAdmin: true}
            },
            {
                path: 'pickups',
                name: 'pickups',
                component: () => import('../views/AdminViews/SchedulePickups.vue'),
                meta: {guardsRouteAdmin: true}
            }
        ]
    },
    {
        path: '/operator',
        component: () => import('../views/OperatorView.vue'),
        children: [
            {
                path: '',
                name: 'operator',
                component: () => import('@/views/OperatorViews/OperatorIndex.vue'),
                meta: {guardsRoute: true}
            },
            {
                path: 'boxlisting',
                name: 'boxlisting',
                component: () => import('@/views/OperatorViews/BoxListing.vue'),
                meta: {guardsRouteOperator: true},
                props: true
            }
        ],
    },
    {
        path: '/about',
        name: 'about',
        component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
    }
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {

    if (to.meta.guardsRoute) {
        if(!store.getters.userAutenticate)
            store.dispatch('loadStorage')
        if(from.name != 'login' && !store.getters.userAutenticate)
            return next({ name: 'login'})
        if(!store.getters.userAutenticate)
            return false
        if (to.name != 'admin' && from.name != 'admin' && store.getters.userRole == "ROLE_ADMIN" ) {
            console.log("admin")
            return next({ name: 'admin'})
        } else if(to.name != 'operator' && from.name != 'operator' && store.getters.userRole == "ROLE_OPERATOR") {
            console.log("operator")
            return next({ name: 'operator'})
        }
    }

    if (to.meta.guardsRouteAdmin) {
        if(store.getters.userRole == "ROLE_ADMIN")
            return next();
        else
            return next({ name: 'login'})
    }

    if (to.meta.guardsRouteOperator) {
        if(store.getters.userRole == "ROLE_OPERATOR")
            return next();
        else
            return next({ name: 'login'})
    }

    return next()
    
  })

export default router
